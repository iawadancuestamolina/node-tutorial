/*/*
  /routes/index.js
*/
const express = require('express');
const router = express.Router();
const path = require('path');
const user = require('../controllers/user');

var ctrPath=path.resolve('controllers/');
const controllerUser  = require(path.join(ctrPath, "user"));

//Middleware para mostrar datos del request
/*router.use (function (req,res,next) {
  console.log(req)
  next();
});
*/

//Analiza la ruta y el protocolo y reacciona en consecuencia
router.get('/',function(req,res){
  res.sendFile(path.resolve('views/index.html'));
});

router.post('/mayor', user.add);
router.post('/mayor', user.isMayor);
router.post('/mayor', function(req, res){
  res.render('isMayor', {
     mayor: req.mayor 
      });
});

module.exports = router;