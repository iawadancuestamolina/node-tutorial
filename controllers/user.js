
    var User = require('../models/user');

    //Create a new user and save it
    add = (req, res, next)=>{
        var user = new User({name: req.body.name, age: req.body.age});
        user.save();
        // res.end();
        next();
    };
    //find all people
    exports.list = function(req, res){
        User.find(function(err, users) {
            res.send(users);
        });
    };

    //find person by id
    exports.find = (function(req, res) {
        User.findOne({_id: req.params.id}, function(error, user) {
            res.send(user);
        })
    });

    isMayor = (function(req, res, next) {
        // console.log(req.body.name);
        if(req.body.age >= 18) {
            req.mayor = true;
        } else {
            req.mayor = false;
        }
        next();
    });


    module.exports = {
        add,
        isMayor,
    }


    
 
