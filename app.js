var express = require('express'),
  mongoose = require('mongoose'),
  http = require('http');
var app = express();

mongoose.connect(
  `mongodb://root:pass12345@localhost:27017/tutorial?authSource=admin`,
  {  useUnifiedTopology: true, useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online`);
  });

const index = require('./routes/index');

const viewPath = __dirname + '/views/';
const ctrlPath = "/controllers";

//introducimos el pug como motor de vistas
app.set('view engine', 'pug');
app.set('views','./views');
app.use(express.urlencoded({extended: true}));
app.use('/', index);

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

